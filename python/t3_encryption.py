import base64
import hashlib
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Protocol.KDF import PBKDF2
import json

# DONE
BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]


# DONE
# The harder way is to use PBDKF2 function, but also the more secure way. If you will use it, you need to have salt.
# As salt use phrase: salt = b"Salty_MrPass_So_Salty"
def get_priv_key(password):
    func = 'harder'
    if func == 'harder':
        salt = b"Salty_MrPass_So_Salty"
        kdf = PBKDF2(password, salt, 64, 1000)
        key = kdf[:32]
        return key
    elif func == 'easier':
        return hashlib.sha256(password)


# DONE
def encrypt(raw_data, password):
    private_key = get_priv_key(password)
    raw = pad(raw_data)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(private_key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw.encode('utf-8')))


# DONE
def decrypt(encrypted_data, password):
    private_key = get_priv_key(password)
    enc = base64.b64decode(encrypted_data)
    iv = enc[:16]
    cipher = AES.new(private_key, AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(enc[16:]))


############################################################
###                                                      ###
###--------------DO NOT TOUCH MAIN FUNCTION--------------###
###                                                      ###
############################################################
if __name__ == "__main__":
    passwd = "JpWp_4W50#3_3nCrYpT10n_T45k"
    with open('./local.json', 'r') as fin:
        data = json.load(fin)
        fin.close()

    print(data)
    print()
    enc = encrypt(str(data), passwd)
    print(enc)
    print()
    dec = decrypt(enc, passwd)
    print(dec)